'use strict';

/*
  motivated as valid isbns such as '9786613249692' are not valid according to 
  the isbn module
*/

return module.exports = {
  strip10,
  strip13,
  normalize10,
  normalize13,
  is10,
  is13,
  checksum10,
  checksum13,
  to10,
  to13,
}

function strip10(isbn) {
  return String(isbn).replace(/x/g, 'X').replace(/[^\dX]/g, '');
}

function strip13(isbn) {
  return String(isbn).replace(/[^\d]/g, '')
}

function normalize10(isbn) {
  isbn = strip10(isbn);
  return is10(isbn) ? isbn : null;
}

function normalize13(isbn) {
  isbn = strip13(isbn);
  return is13(isbn) ? isbn : null;
}


function is10(isbn) {
  isbn = strip10(isbn);
  return Boolean(isbn && /^\d{9}[\dX]$/.test(isbn) && checksum10(isbn)===isbn[9]);
}

function is13(isbn) {
  isbn = strip13(isbn);
  return Boolean(isbn && /^\d{13}$/.test(isbn) && checksum13(isbn)===isbn[12])
}

function checksum10(isbn) {
  isbn = strip10(isbn);
  if(!isbn) return null;
  
  var digits = isbn.split('').slice(0, 9).map(Number);
  
  var transformed = digits.map(function(d, i) {
    return d*(10 - i);
  });
  
  var sum = 0;
  transformed.forEach(function(d) {
    return sum+=d;
  });
  
  var c = (11-sum%11)%11;
  var s = c===10 ? 'X' : String(c);
  
  return s;
}

function checksum13(isbn) {
  isbn = strip13(isbn);
  if(!isbn) return null;
  
  var digits = isbn.split('').map(Number);
  var block = digits.slice(0, 12);
  var transformed = block.map(function(d, i) {
    return (i%2===0 ? 1 : 3) * d;
  });
  
  var sum = 0;
  transformed.forEach(function(d) {
    return sum +=d;
  });
  
  var r = sum%10;
  
  var c = (10 - r)%10;
  
  return String(c);
}


function to10(isbn) {
  if(is10(isbn)) return isbn;
  if(!is13(isbn)) return null;
  if(!/^978/.test(isbn)) return null;
  
  isbn = normalize13(isbn);

  var block = isbn.slice(3);
  return block.slice(0, 9) + checksum10(block);
}

function to13(isbn) {
  if(is13(isbn)) return isbn;
  if(!is10(isbn)) return null;

  isbn = normalize10(isbn);
  
  var block = '978' + isbn;
  return block.slice(0, 12) + checksum13(block);
}

